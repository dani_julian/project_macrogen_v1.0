#! /bin/python
#@dani_julian

#Script to parse bam file and get the counts per position of a target region

#0. Packages
import sys
import re
from typing import AsyncGenerator

#1. Input
sam = open(sys.argv[1], "rt")

for line in sam:
    if not line.startswith("@"): #avoid headers
            line = line.split("\t")
            if "i:0" in line[12]:
                continue
            else: 
                target = line[2].split("_")[1].rstrip()
                if "^" not in line[12]:
                    positions = re.split('A|T|G|C',line[12].split(":")[2])
                    count = int(line[3])-1
                    for i in range(0,len(positions)-1):
                        if positions[i]:
                            print(target + "\t" + str(int(positions[i])+1+count))
                            count += int(positions[i])+1
                        # if "^" in positions[i]: # in case of deletions or insertions the read is not counted
                        #     count += int(positions[i].replace("^",""))+1
                        #     continue
                        # else:
                        #     print(target + "\t" + str(int(positions[i])+1+count))
                        #     count += int(positions[i])+1
                        

sam.close()
