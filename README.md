# Macrogen project

## 0.- Download repository
```shell
git clone git@gitlab.com:dani_julian/project_macrogen_v1.0.git
```
## 1.- Introduction
This pipeline provides the opportunity of classificate (with GAIA) samples from a known set of target sequences. It is also possbile to merge (with NGmerge) the input paired-end sequences and classify them as well. The output are GAIA results (html, OTU tables, etc), histogram with the amount of SNPs per target sequence and a FASTA file containing most representitive sequences in the sample.
### 1.1- Version 1.0

At this moment, the pipeline is not able to plot INDELS in the resulted histogram

## 2.- Usage

| Argument | Explanation |
| :---: |     :---: |
| -h | Show this help text |
| -i | Directory with input fastq files (paired-end) | 
| -d | Database with the target sequences to use for the alignment | 
| -m | Option to merge paired-end reads |
| -o | Output directory to store results | 

## 3.- Example
```bash
bash project_macrogen_v1.0/Pipeline_final_v1.1.sh -i Raw_data/ -d index_target_sin_repeticiones/target_sequences_2.fasta -m -o Output_Example_1

```

