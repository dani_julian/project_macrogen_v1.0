PASS	Basic Statistics	Sample_1_1.fastq.gz
PASS	Per base sequence quality	Sample_1_1.fastq.gz
PASS	Per sequence quality scores	Sample_1_1.fastq.gz
FAIL	Per base sequence content	Sample_1_1.fastq.gz
FAIL	Per sequence GC content	Sample_1_1.fastq.gz
PASS	Per base N content	Sample_1_1.fastq.gz
WARN	Sequence Length Distribution	Sample_1_1.fastq.gz
FAIL	Sequence Duplication Levels	Sample_1_1.fastq.gz
FAIL	Overrepresented sequences	Sample_1_1.fastq.gz
PASS	Adapter Content	Sample_1_1.fastq.gz
FAIL	Kmer Content	Sample_1_1.fastq.gz
