PASS	Basic Statistics	Sample_1_2.tr.fq.gz
PASS	Per base sequence quality	Sample_1_2.tr.fq.gz
PASS	Per sequence quality scores	Sample_1_2.tr.fq.gz
FAIL	Per base sequence content	Sample_1_2.tr.fq.gz
FAIL	Per sequence GC content	Sample_1_2.tr.fq.gz
PASS	Per base N content	Sample_1_2.tr.fq.gz
WARN	Sequence Length Distribution	Sample_1_2.tr.fq.gz
FAIL	Sequence Duplication Levels	Sample_1_2.tr.fq.gz
FAIL	Overrepresented sequences	Sample_1_2.tr.fq.gz
PASS	Adapter Content	Sample_1_2.tr.fq.gz
WARN	Kmer Content	Sample_1_2.tr.fq.gz
