#!/bin/bash
#@dani_julian

usage="$(basename "$0") [-h] [-i input] [-d database] [-m] [-o output]

    -h  Show this help text
    -i  Directory with input fastq files (paired-end)
    -d  Database with the target sequences to use for the alignment
    -m  Option to merge paired-end reads
    -o  Output directory to store results"

while getopts "i:o:d:h:m" opt; do
  case ${opt} in
    h ) echo "$usage"; exit;;
    i ) input=$OPTARG;;
    d ) database=$OPTARG;;
    o ) DIR=$OPTARG;;
    m ) merging="Yes";;
    : ) echo "Invalid option: $OPTARG requires an argument" >&2; echo "$usage" >&2; exit 1;;
    \? ) echo "Invalid option: $OPTARG. It is required an argument" >&2; echo "$usage" >&2; exit 1;;
  esac
done

#if no option is passed
#if [ $OPTIND -eq 1 ]; then echo "No options were passed. It is required the option -s (samples), which is the absolute path of the directory where strains data (proteins, genome and annoations) are stored. The name of this folder, must be the name of the acual species separate by _" && exit 1; fi
#mandatory argumnents
if [ ! "$input" ] || [ ! "$database" ]; then
  echo "arguments -d and -i must be provided"
  echo "$usage" >&2; exit 1
fi

#Get the path to this pipeline
path=$(realpath $0 | awk 'BEGIN{FS=OFS="/"} {$NF=""; print $0}')

#IMPLEMENTACIONES
#warning si el directorio esta vacio, si no tiene fastq, o si no hay paired-end Usar lo de return=1, etc para salir de la pipe
#mostrar el timepo de lo que dura cada cosa
#check que los nombres de los 1 y 2 son iguales para hacer bien el merging
#orden de los argumentos, ver si funcionan si los cambio el orden

#function to create table
create_statistics () {
    analysis=$(echo $DIR | awk -F "/" '{print $NF}' | cut -d "_" -f 1)
    sample=$1
    product=$(echo $sample | fold -w 1 | head -n 2 | tail -n 1) #they usually are CV or CL
    raw=$(find "/Synology/daniel/Macrogen/Raw_data/"$analysis | grep $sample"_1")
    raw_reads=$(zcat $raw | paste - - - - | wc -l)
    trimmed=$(find $DIR"/Trimmed_"$DIR"_paired-reads" | grep "Trimmed_files" | grep $sample"_1")
    trimmed_reads=$(zcat $trimmed | paste - - - - | wc -l)
    remained_trimmed=$(echo "scale=5; ($trimmed_reads/$raw_reads)*100" | bc)
    merged=$(find $DIR"/Raw_data_merging" | grep $sample"_merged")
    merged_reads=$(zcat $merged | paste - - - - | wc -l)
    remained_merged=$(echo "scale=5; ($merged_reads/$trimmed_reads)*100" | bc)

    #Get only reads with 100% of identity and coverage and covers a whole target
    query_fasta=$(find $DIR"/Consensus_files_merged-reads" | grep $sample"_100_ident")
    if [ -s $query_fasta ]; then
    blastn -query $query_fasta -subject "/Synology/daniel/Macrogen/Databases/All_targets/V_product/V_database_revised.fasta" -outfmt "6 qseqid sseqid pident qlen qstart qend sstart qcovs" | while read line; do
    l=$(echo $line | cut -d " " -f 4)
    t=$(echo $line | cut -d " " -f 2)
    lt=$(grep -w $t "/Synology/daniel/Macrogen/Databases/All_targets/V_product/Targets_lenghts.tsv" | cut -f 2)
    r=$(echo $line | cut -d " " -f 1)
    p=$(echo "scale=10; $l/$lt" | bc)
    if [[ "$p" == 1.0000000000 ]]; then echo -e $sample"\t"$(echo $r | sed 's/;size=/\\t/')"\t"$t"\t"$lt; fi; done > $DIR"/tmp.txt"
    total_reads_100=$(awk -F "\t" '{SUM+=$3}END{print SUM}' $DIR"/tmp.txt"); else total_reads_100=0; fi
    if [ -z "$total_reads_100" ]; then total_reads_100=0; fi
    remained_total_reads_100=$(echo "scale=5; ($total_reads_100/$merged_reads)*100" | bc)

    #Write final line per sample
    echo -e $sample"\t"$product"\t"$raw_reads"\t"$trimmed_reads"\t"$remained_trimmed"\t"$merged_reads"\t"$remained_merged"\t"$total_reads_100"\t"$remained_total_reads_100

}

#time
start=`date +%s`
mkdir $DIR

#1. Quality check of input files and trimming

#1.1. Pairs.json construction (before trimming)
ls $input | sort -V | sed '1~2 s/.*/{"forward":"&"/ ; 2~2 s/.*/"reverse":"&"},/' | paste - - | tr "\t" "," | sed '1s/^/[/;$s/.$/]/' > $DIR"/pairs.json"

#1.2 Trimming with trimmomtatic
python2 /scripts/trimming.py/ -g trimmomatic -l 100 -z -P illumina -p $DIR"/pairs.json" -i $input -t 20 -z -q 1 -o $DIR"/Trimmed_"$DIR"_paired-reads"
#python2 /scripts/trimming.py/ -l 100 -P illumina -a /scripts/trimming.py/libs/adapters.bbduk.fa -t 20 -z -q 1 -k 5 -p $DIR"/pairs.json" -i $input -o $DIR"/Trimmed_"$DIR"_paired-reads"
#1.2. Merging option
if [[ $merging ]]; then mkdir $DIR"/Raw_data_merging" && ls $DIR"/Trimmed_"$DIR"_paired-reads/Trimmed_files" | sort -V | paste - - | while read line; do input_1=$(echo $line | cut -d " " -f 1) && input_2=$(echo $line | cut -d " " -f 2) && $path"/scripts/NGmerge/NGmerge" -1 $DIR"/Trimmed_"$DIR"_paired-reads/Trimmed_files/"$input_1 -2 $DIR"/Trimmed_"$DIR"_paired-reads/Trimmed_files/"$input_2 -d -o $DIR"/Raw_data_merging/"$(echo $input_1 | sed 's/_1.tr.fq.gz/_merged.fastq.gz/'); done; fi
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error in merging fastqs process" >&2 ; exit 1 ; fi

#1.3 Pairs.json construction (after trimming)
ls $DIR"/Trimmed_"$DIR"_paired-reads/Trimmed_files" | sort -V | sed '1~2 s/.*/{"forward":"&"/ ; 2~2 s/.*/"reverse":"&"},/' | paste - - | tr "\t" "," | sed '1s/^/[/;$s/.$/]/' > $DIR"/pairs.json"
if [[ $merging ]]; then ls $DIR"/Raw_data_merging" | sed 's/.*/{"forward":"&","reverse":""},/' | sed '1s/^/[/;$s/.$/]/' > $DIR"/pairs_merging.json"; fi
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error constructing pairs.json after trimming" >&2 ; exit 1 ; fi


#2. GAIA
bash /Synology/daniel/Macrogen/gaia2/main.sh --input $DIR"/Trimmed_"$DIR"_paired-reads/Trimmed_files" --pairs $DIR"/pairs.json" --index $database --mode rrna --threads 20 --html --output $DIR"/GAIA_output_paired-reads"
#find $DIR"/GAIA_output_paired-reads" | grep "parsed2.txt" | while read line; do parsing_parsed2 $line; done
if [[ $merging ]]; then bash /Synology/daniel/Macrogen/gaia2/main.sh --input $DIR"/Raw_data_merging" --pairs $DIR"/pairs_merging.json" --index $database --mode rrna --threads 20 --html --output $DIR"/GAIA_output_merged-reads";fi
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error in GAIA" >&2 ; exit 1 ; fi


#3. Vsearch to get consensus seq
mkdir $DIR"/Consensus_files_paired-reads" $DIR"/Fasta_100_ident_cov_paired-reads"
mkdir $DIR"/Consensus_files_merged-reads" $DIR"/Fasta_100_ident_cov_merged-reads"

#3.1 Create fasta files from reads that has 100% of identity and coverage
find $DIR"/GAIA_output_paired-reads/" | grep "alignments.bam" | while read line; do name=$(echo $line | awk 'BEGIN{FS=OFS="/"} {print $(NF-1)}' | sed 's/$/_100_ident_and_cov.fasta/') && samtools view $line | awk '{l=length($10)} {if($12=="NM:i:0" && $13 == "MD:Z:"l) {print $0}}' | awk 'a[$1]++{print ">"$1"\n"$10}' > $DIR"/Fasta_100_ident_cov_paired-reads/"$name;done
find $DIR"/GAIA_output_merged-reads/" | grep "alignments.bam" | while read line; do name=$(echo $line | awk 'BEGIN{FS=OFS="/"} {print $(NF-1)}' | sed 's/_merged/_100_ident_and_cov.fasta/') && samtools view $line | awk '{l=length($10)} {if($12=="NM:i:0" && $13 == "MD:Z:"l) {print ">"$1"\n"$10}}' > $DIR"/Fasta_100_ident_cov_merged-reads/"$name;done

#3.2 Vsearch commnads
find $DIR"/Fasta_100_ident_cov_paired-reads" | grep ".fasta" | while read line; do name=$(echo $line | awk 'BEGIN{FS=OFS="/"} {print $NF}' | sed 's/.fasta//') && vsearch --cluster_size $line --threads 10 --id 1 --strand plus --sizeout --centroids $DIR"/Consensus_files_paired-reads/"$name"_vsearch_output.fasta"; done
find $DIR"/Fasta_100_ident_cov_merged-reads" | grep ".fasta" | while read line; do name=$(echo $line | awk 'BEGIN{FS=OFS="/"} {print $NF}' | sed 's/.fasta//') && vsearch --cluster_size $line --threads 10 --id 1 --strand plus --sizeout --centroids $DIR"/Consensus_files_merged-reads/"$name"_vsearch_output.fasta"; done
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error finding consensus read" >&2 ; exit 1 ; fi

#4. Histogram of missmatches in target regions
#4.1 Parsing bam file to construct table with positions and missmatches per target
mkdir $DIR"/Histogram_missmatches_paired-reads"
ls $DIR"/GAIA_output_paired-reads/" | grep -Ev "results.html|GAIA2_tmpDir" | while read line; do mkdir $DIR"/Histogram_missmatches_paired-reads/"$line && samtools view -h -o $DIR"/Histogram_missmatches_paired-reads/"$line"/alignments.sam" $DIR"/GAIA_output_paired-reads/"$line"/alignments.bam" && cp $path"scripts/Parsing_histogram.py" $path"scripts/Histogram_creator.R" $DIR"/Histogram_missmatches_paired-reads/"$line && python3 $DIR"/Histogram_missmatches_paired-reads/"$line"/Parsing_histogram.py" $DIR"/Histogram_missmatches_paired-reads/"$line"/alignments.sam" > $DIR"/Histogram_missmatches_paired-reads/"$line"/histogram_input.txt" && sed -i 's/VE7/VE_new/; s/VRT3/VRT_new/' $DIR"/Histogram_missmatches_paired-reads/"$line"/histogram_input.txt" && Rscript $DIR"/Histogram_missmatches_paired-reads/"$line"/Histogram_creator.R" "histogram_input.txt" $DIR"/Histogram_missmatches_paired-reads/"$line && rm $DIR"/Histogram_missmatches_paired-reads/"$line"/alignments.sam" $DIR"/Histogram_missmatches_paired-reads/"$line"/Histogram_creator.R" $DIR"/Histogram_missmatches_paired-reads/"$line"/Parsing_histogram.py"; done
mkdir $DIR"/Histogram_missmatches_merged-reads"
ls $DIR"/GAIA_output_merged-reads/" | grep -Ev "results.html|GAIA2_tmpDir" | while read line; do name=$(echo $DIR"/Histogram_missmatches_merged-reads/"$line | sed 's/_merged.fastq.gz//' | awk -F "/" '{print $NF}') && mkdir $DIR"/Histogram_missmatches_merged-reads/"$name && samtools view -h -o $DIR"/Histogram_missmatches_merged-reads/"$name"/alignments.sam" $DIR"/GAIA_output_merged-reads/"$line"/alignments.bam" && cp $path"/scripts/Parsing_histogram.py" $path"/scripts/Histogram_creator.R" $DIR"/Histogram_missmatches_merged-reads/"$name && python3 $DIR"/Histogram_missmatches_merged-reads/"$name"/Parsing_histogram.py" $DIR"/Histogram_missmatches_merged-reads/"$name"/alignments.sam" > $DIR"/Histogram_missmatches_merged-reads/"$name"/histogram_input.txt" && sed -i 's/VE7/VE_new/; s/VRT3/VRT_new/' $DIR"/Histogram_missmatches_merged-reads/"$name"/histogram_input.txt" && Rscript $DIR"/Histogram_missmatches_merged-reads/"$name"/Histogram_creator.R" "histogram_input.txt" $DIR"/Histogram_missmatches_merged-reads/"$name && rm $DIR"/Histogram_missmatches_merged-reads/"$name"/alignments.sam" $DIR"/Histogram_missmatches_merged-reads/"$name"/Histogram_creator.R" $DIR"/Histogram_missmatches_merged-reads/"$name"/Parsing_histogram.py"; done
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error creating histogram plots" >&2 ; exit 1 ; fi

#Create statistics table
ls $DIR"/Raw_data_merging/" | sed 's/_merged.fastq.gz//' | sort -V | while read line; do create_statistics $line; done > $DIR"/statistics.tsv"
sed -i 's/\./,/g' $DIR"/statistics.tsv"
#header
sed -i '1 iSample\tProduct\t# Raw reads\t#Trimmed reads\t% Remained reads\t# Merged reads\t% Merged reads\t# Classified 100% ident\/cov\/complete-target\t% Classified 100% ident\/cov\/complete-target\t# Classified 93% ident\t% Classified 93% ident' $DIR"/statistics.tsv"
ssconvert $DIR"/statistics.tsv" $DIR"/statistics.xlsx"
EXIT_CODE=$?
if [[ $EXIT_CODE -gt 0 ]]; then echo "Error creating statistics.xlsx" >&2 ; exit 1 ; fi

#log file
end=`date +%s`
total_time=$(bc <<< "scale=5;($end-$start)/60" | awk '{printf "%.2f",$1}')
echo -e "Run time:\t"$total_time"mins" > $DIR"/log.file"