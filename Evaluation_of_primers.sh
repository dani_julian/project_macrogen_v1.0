#!/bin/bash
#@dani_julian

usage="$(basename "$0") [-h] [-p primers] [-d database] [-o output]

    -h  Show this help text
    -p  Fasta file with the primers to be evaluated
    -d  Database with the sequences to be aligned our primers (for example mitochondria or chloroplast sequences)
    -o  Output directory to store results"

while getopts "p:o:d:h" opt; do
  case ${opt} in
    h ) echo "$usage"; exit;;
    p ) primers=$OPTARG;;
    d ) database=$OPTARG;;
    o ) DIR=$OPTARG;;
    : ) echo "Invalid option: $OPTARG requires an argument" >&2; echo "$usage" >&2; exit 1;;
    \? ) echo "Invalid option: $OPTARG. It is required an argument" >&2; echo "$usage" >&2; exit 1;;
  esac
done

#if no option is passed
#if [ $OPTIND -eq 1 ]; then echo "No options were passed. It is required the option -s (samples), which is the absolute path of the directory where strains data (proteins, genome and annoations) are stored. The name of this folder, must be the name of the acual species separate by _" && exit 1; fi
#mandatory argumnents
if [ ! "$primers" ] || [ ! "$database" ]; then
  echo "arguments -d and -p must be provided"
  echo "$usage" >&2; exit 1
fi

#Get the path to this pipeline
path=$(echo $0 | awk 'BEGIN{FS=OFS="/"} {$NF=""; print "./"$0}')

#time
start=`date +%s
echo "Start of analysis: " $(date) > $DIR"/log.file"

#0. Create output directory
mkdir $DIR

#1. Create primers file without degenarated bases
perl $path"/scripts/Cambio_bases_degenerated.pl" $primers && mv non_deg_probe_list.txt $DIR

#2. Blastn against selected database
blastn -num_threads 6 -outfmt "6 qseqid sseqid pident qcovs evalue bitscore staxids" -db $database -query $DIR"/non_deg_probe_list.txt" -task blastn > $DIR"/Results_blast.out"
#2.1 Filter lines with more than 2 missmatches, more than 0 gaps and less than 90% of query coverage
awk -F "\t" '$5 < 3 && $6 < 1 && $13 >= 90' $DIR"/Results_blast_no_filtered.out" > $DIR"/tmp" && mv $DIR"/tmp" $DIR"/Results_blast.out"

#3. Filter the hits that have matched against sequences of the same species
cat $DIR"/Results_blast.out" | awk 'BEGIN{FS=OFS="\t"} {split($2,a,"_"); {print $1,a[1] }}' | sort -u > $DIR"/Results_blast_unique_species.out"

#4. Get taxons of our hits (with taxonkit)
cat $DIR"/Results_blast_unique_species.out" | cut -f 2 | taxonkit lineage | taxonkit reformat | cut -f 3 > $DIR"/List_of_taxons.txt"

#5. Paste all info in a prefinal table
python3 $path"/Get_sequences_of_primers.py" $DIR"/non_deg_probe_list.txt" $DIR"/Results_blast_unique_species.out" | paste - $DIR"/List_of_taxons.txt" > $DIR"/Pre_final_table.tsv"

#6. Construction of final table
python3 $path"/Construction_final_table.py" $DIR"/Pre_final_table.tsv" > $DIR"/Final_table.tsv"

#log file
end=`date +%s`
echo "End of analysis: " $(date) >> $DIR"/log.file"
total_time=$(bc <<< "scale=5;($end-$start)/60" | awk '{printf "%.2f",$1}')
echo -e "Complete NIPT Analysis\t"$total_time"mins" >> $DIR"/log.file"